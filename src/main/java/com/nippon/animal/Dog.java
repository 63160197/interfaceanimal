/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Dog extends LandAnimal {

    private String name;

    public Dog(String name) {
        super("Dog", 4);
        this.name = name;
    }

    @Override
    public void speak() {
    }

    @Override
    public void eat() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void run() {
        System.out.println("Dog : " + name + " run");
    }

}
