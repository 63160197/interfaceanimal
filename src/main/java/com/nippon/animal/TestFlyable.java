/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("Nong");
        Plane plane = new Plane("Engine 1");
        Bird bird = new Bird("Suay");
        Dog dog = new Dog("Mao");
        Human human = new Human("Sanwa");
        Cat cat = new Cat("Maew");
        Fish fish = new Fish("Pong");
        Crab crab = new Crab ("Kakae");
        Crocodile crocodile = new Crocodile ("Nee");
        Snake snake = new Snake ("wang");
        
        Flyable[] flyables = {bat, bird, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                System.out.println("-----Animal Fly-----");
                p.startEngine();
                p.run();

            }
            f.fly();

        }
        System.out.println("-----End of engine-----");
        Runable[] runables = {dog, cat,human, plane};
        for (Runable r : runables) {
            r.run();
        }
        System.out.println("-----Run-----");
        
        Swimable[] swimables = {fish,crab};
        for (Swimable s : swimables){
            s.swim();
        }
        System.out.println("-----Swim-----");
        
        Crawlable [] crawlables = {snake,crocodile};
        for(Crawlable c : crawlables){
            c.crawl();
        }
        System.out.println("-----Crawl-----");
        System.out.println("---------------BYE---------------");
    }

}
