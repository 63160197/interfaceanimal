/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Bat extends Poultry {

    private String name;

    public Bat(String name) {
        super("Bat",2);
        this.name=name;
    }

    @Override
    public void speak() {
    }

    @Override
    public void eat() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void fly() {
        System.out.println("Bat : " + name + " fly");
    }

}
