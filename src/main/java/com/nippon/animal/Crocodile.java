/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Crocodile extends Reptile {

    private String name;

    public Crocodile(String name) {
        super("Crocodile",4);
        this.name=name;
    }

    @Override
    public void speak() {
    }

    @Override
    public void eat() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile : " + name + " crawl");
    }

}
