/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public abstract class Animal {

    String name;
    private int numberofleg;

    public Animal(String name, int numberofleg) {
        this.name = name;
        this.numberofleg = numberofleg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberofleg() {
        return numberofleg;
    }

    public void setNumberofleg(int numberofleg) {
        this.numberofleg = numberofleg;
    }

    public abstract void speak();

    public abstract void eat();

    public abstract void sleep();
}
