/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Plane extends Vahicle implements Flyable, Runable {

    private String name;

    Plane(String name) {
        super("engine");
        this.name = name;

    }

    @Override
    public void startEngine() {
        System.out.println("Plane : Start Engine");
    }

    @Override
    public void stopEngine() {
    }

    @Override
    public void raiseSpeed() {
    }

    @Override
    public void applyBreak() {
    }

    @Override
    public void fly() {
        System.out.println("Plane : " + name + " fly");
    }

    @Override
    public void run() {
        System.out.println("Plane : " + name + " run");
    }

}
