/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Fish extends Aqualtic {

    private String name;

    public Fish(String name) {
        super("Fish",0);
        this.name = name;
    }

    @Override
    public void speak() {
    }

    @Override
    public void eat() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void swim() {
        System.out.println("Fish : " + name + " swim");
    }
}
