/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.animal;

/**
 *
 * @author Nippon
 */
public class Crab extends Aqualtic {

    private String name;

    public Crab(String name) {
        super("Carb",6);
        this.name = name;
    }

    @Override
    public void speak() {
    }

    @Override
    public void eat() {
    }

    @Override
    public void sleep() {
    }

    @Override
    public void swim() {
        System.out.println("Crab : " + name + " swim");

    }
}
